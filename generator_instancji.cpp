#include<iostream>
#include<ctime>
#include<vector>
#include<cstdlib>
#include<cstring>
#include<fstream>
#include<algorithm>
#include"generator_instancji.h"

using namespace std;

int prev_maint_stop1;
int prev_maint_stop2;

void generuj_instancje(Globalne &g) {
    vector<task>().swap(g.op1);
    vector<task>().swap(g.op2);
    vector<task>().swap(g.maintenance1);
    vector<task>().swap(g.maintenance2);
    prev_maint_stop1 =0;
    prev_maint_stop2 =0;
    for(int i =0 ; i<500; i++){
        g.distr1[i] = 0;
        g.distr2[i] = 0;
    }

	int sumaM1 = 0;
	int sumaM2 = 0;

	for (int i = 0; i < n; i++){ //losowanie n wartosci
        task p1;                    //tworzenie op1 i-tego zadania
		p1.time =rand() % 20 + 1;
		p1.zadanie = i+1;
        strcpy(p1.typ, "op");
        g.op1.push_back(p1);          //dodanie operacji do wektora

        task p2;//druga operacja jest na przeciwnej maszynie
		p2.time =rand() % 20 + 1;
		p2.zadanie = i+1;
        strcpy(p2.typ, "op");
		g.op2.push_back(p2);

        sumaM1 += p1.time;
        sumaM2 += p2.time;
	}

	for (int i = 0; i < liczba_main; i++){
        task m1=paraMA(g,1);
		g.maintenance1.push_back(m1);
		task m2=paraMA(g,2);
		g.maintenance2.push_back(m2);
	}
};

task paraMA(Globalne &g, int nr){
	int prev_maint_stop;
	if (nr == 1) {
        prev_maint_stop = prev_maint_stop1;
    } else {
        prev_maint_stop = prev_maint_stop2;
    }
    task m;
    strcpy(m.typ, "m_");
    m.machine = nr;
    bool good = false;
    int start=0;
    int duration=0;

    while (!good) {
        start = prev_maint_stop+rand() % 80;
        duration = rand() % 20 + 1;//losowanie czasu trwania
        good = true;
        for (int i = start; i < start + duration; i++) {
            if (g.distr1[i] == 1 || g.distr2[i] == 1){
                good = false;
                break;
            }
        }
    }

    for (int i = start; i < start + duration; i++){
        if (nr == 1) {
            g.distr1[i] = 1;//wpisywanie maintenance'u do tablicy
        } else {
            g.distr2[i] = 1;
        }
    }

    m.start = start;
    m.time = duration;
    m.endtime = start + duration;
	if (nr == 1) {
        prev_maint_stop1 = start + duration;
    } else {
        prev_maint_stop2 = start + duration;
    }

    return m;
};


void zapis(Globalne &g,int nr) {

    ofstream file;
	char tytul[20];
	snprintf(tytul, sizeof tytul, "instancja%03d.txt",nr);

	file.open(tytul);
	file << "**** " << nr << " ****" << endl;

	for (unsigned int i = 0; i<g.op1.size(); i++){
        file << i+1 <<";1;"<< g.op1[i].time << ";2;" << g.op2[i].time << endl;
    }
    file << endl;

    for (unsigned int j = 0; j < g.maintenance2.size(); j++){
		file << j+1 << ";" << 1 << ";" << g.maintenance1[j].time<< ";" << g.maintenance1[j].start << endl;
		file << j+1 << ";" << 2 << ";" << g.maintenance2[j].time << ";" << g.maintenance2[j].start << endl;
	}
	file.close();

};
