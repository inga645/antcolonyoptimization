#ifndef GENERATOR_INSTANCJI_H_INCLUDED
#define GENERATOR_INSTANCJI_H_INCLUDED

#include<iostream>
#include<ctime>
#include<vector>
#include<cstdlib>
#include<fstream>
#include"globalne.h"

using namespace std;

void generuj_instancje(Globalne&);
void zapis(Globalne&,int);
task paraMA(Globalne&,int);

#endif // GENERATOR_INSTANCJI_H_INCLUDED
