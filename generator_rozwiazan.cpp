#include<iostream>
#include<string.h>
#include<fstream>
#include<vector>

#include "globalne.h"
#include "generator_rozwiazan.h"
#include "kolo_fortuny.h"
#include "generator_instancji.h"
using namespace std;

unsigned int czas_tmp;
vector<task> szereg1;
vector<task> szereg2;
int l_maint1;
int l_maint2;

void generuj_rozwiazanie(Globalne &g, bool mrowka_widzi, int iteracja){

    for(int i=0; i<n; i++){ //ustawianie czasow, w ktorych dostepne sa zadania
            g.dostepne1[i] = 0;
            g.dostepne2[i] = -1;
    }

    g.czas_maszyny1 = 0;
    g.czas_maszyny2 = 0;
    l_maint1 =0;
    l_maint2 = 0;
    int pkt1, pkt2;
    int kol1[n];
    int kol2[n];
    czas_tmp = 0;

    for(int ktora_operacja=0; ktora_operacja<n; ktora_operacja++){
        if(ktora_operacja==0){
            pkt1 = losuj_slepo(g,1);
            wstaw(g,pkt1,1);
            kol1[ktora_operacja] = pkt1;
        } else {
            if (mrowka_widzi==true) {
                pkt1 = losuj(g,1,pkt1);
                wstaw(g,pkt1,1);
                kol1[ktora_operacja] = pkt1;
            } else {
                pkt1 = losuj_slepo(g,1);
                wstaw(g,pkt1,1);
                kol1[ktora_operacja] = pkt1;
            }
        }
    }

    for(int ktora_operacja=0; ktora_operacja<n; ktora_operacja++){
        if(ktora_operacja==0){
            pkt2 = losuj_slepo(g,2);
            wstaw(g,pkt2,2);
            kol2[ktora_operacja] = pkt2;
        } else {
            if (mrowka_widzi==true) {
                pkt2 = losuj(g,2,pkt2);
                wstaw(g,pkt2,2);
                kol2[ktora_operacja] = pkt2;
            } else {
                pkt2 = losuj_slepo(g,2);
                wstaw(g,pkt2,2);
                kol2[ktora_operacja] = pkt2;
            }
        }
    }

    for(int op=0;op<n-1;op++){
        g.tablica_przejscM1[kol1[op]][kol1[op+1]] += feromon;
        g.tablica_przejscM2[kol2[op]][kol2[op+1]] += feromon;
    }

    if(iteracja==0){
        g.czas_losowy = czas_tmp;
        g.najlepszy_czas = czas_tmp;
        g.najlepsze_rozwiazanieM1 = szereg1;
        g.najlepsze_rozwiazanieM2 = szereg2;
    }
    if(czas_tmp < g.najlepszy_czas){
        g.najlepszy_czas = czas_tmp;
        g.najlepsze_rozwiazanieM1.clear();
        g.najlepsze_rozwiazanieM1 = szereg1;
        g.najlepsze_rozwiazanieM2.clear();
        g.najlepsze_rozwiazanieM2 = szereg2;
    }

    vector<task>().swap(szereg1);
    vector<task>().swap(szereg2);
}


void wstaw(Globalne &g, int punkt, int numer){
    if(numer==1){
        if((g.op1[punkt].time+g.czas_maszyny1 <= g.maintenance1[l_maint1].start)||(l_maint1>=liczba_main)){
            g.op1[punkt].start = g.czas_maszyny1;
            g.czas_maszyny1 += g.op1[punkt].time;
            g.op1[punkt].endtime = g.czas_maszyny1;
            czas_tmp += g.op1[punkt].endtime;
            szereg1.push_back(g.op1[punkt]);
            g.dostepne1[punkt] = -1;
            g.dostepne2[punkt] = g.op1[punkt].endtime;
        }
        else{
            while(l_maint1<liczba_main){
                szereg1.push_back(g.maintenance1[l_maint1]);
                g.czas_maszyny1 = g.maintenance1[l_maint1].endtime;
                l_maint1++;
                if((g.op1[punkt].time+g.czas_maszyny1 <= g.maintenance1[l_maint1].start)||(l_maint1>=liczba_main)){
                    g.op1[punkt].start = g.czas_maszyny1;
                    g.czas_maszyny1 += g.op1[punkt].time;
                    g.op1[punkt].endtime = g.czas_maszyny1;
                    czas_tmp += g.op1[punkt].endtime;
                    szereg1.push_back(g.op1[punkt]);
                    g.dostepne1[punkt] = -1;
                    g.dostepne2[punkt] = g.op1[punkt].endtime;
                    break;
                }
            }
        }
    } else {
        if((g.op2[punkt].time+g.czas_maszyny2 <= g.maintenance2[l_maint2].start)||(l_maint2>=liczba_main)){
            g.op2[punkt].start = g.czas_maszyny2;
            g.czas_maszyny2 += g.op2[punkt].time;
            g.op2[punkt].endtime = g.czas_maszyny2;
            czas_tmp += g.op2[punkt].endtime;
            szereg2.push_back(g.op2[punkt]);
            g.dostepne2[punkt] = -1;
        } else {
            while(l_maint2<liczba_main){
                szereg2.push_back(g.maintenance2[l_maint2]);
                g.czas_maszyny2 = g.maintenance2[l_maint2].endtime;
                l_maint2++;
                if ((g.op2[punkt].time+g.czas_maszyny2 <= g.maintenance2[l_maint2].start)||(l_maint2>=liczba_main)){
                    g.op2[punkt].start = g.czas_maszyny2;
                    g.czas_maszyny2 += g.op2[punkt].time;
                    g.op2[punkt].endtime = g.czas_maszyny2;
                    czas_tmp += g.op2[punkt].endtime;
                    szereg2.push_back(g.op2[punkt]);
                    g.dostepne2[punkt] = -1;
                    break;
                }
            }
        }
    }
}


void zapisz_rozw(Globalne &g,int nr){
    ofstream file;
    char tytul[21];
	snprintf(tytul, sizeof tytul, "uszeregowanie%03d.txt",nr);
	file.open(tytul);
    file << "**** " << nr << " ****" << endl;
    file << g.czas_losowy << "\t" << g.najlepszy_czas<<endl;
    int tmp1 =0,idle1=0, idlet1=0,maint1=0,m1count=0, m2count=0, tmp2=0, idle2=0, idlet2=0,maint2=0;
    file << "\nM1: ";
    for(int i=0; i<g.najlepsze_rozwiazanieM1.size(); i++){
        if(g.najlepsze_rozwiazanieM1[i].start>tmp1){
            file<<"idle_"<<idle1+1<<"_M1, "<<tmp1<<", "<<g.najlepsze_rozwiazanieM1[i].start-tmp1<<"; ";
            idle1++;
            idlet1+=g.najlepsze_rozwiazanieM1[i].start-tmp1;
            tmp1 = g.najlepsze_rozwiazanieM1[i].start;
        }
        if(strcmp(g.najlepsze_rozwiazanieM1[i].typ,"m_")==0){
                m1count++;
            file<<"maint"<<m1count<<"_M1,"<<g.najlepsze_rozwiazanieM1[i].start<<", "<<g.najlepsze_rozwiazanieM1[i].time<<"; ";
            maint1 += g.najlepsze_rozwiazanieM1[i].time;
        }
        else if(strcmp(g.najlepsze_rozwiazanieM1[i].typ,"op")==0)
            file<<"op1_"<<g.najlepsze_rozwiazanieM1[i].zadanie<<", "<<g.najlepsze_rozwiazanieM1[i].start<<", "<<g.najlepsze_rozwiazanieM1[i].time<<"; ";
        tmp1 = g.najlepsze_rozwiazanieM1[i].endtime;
    }

    file << "\nM2: ";
    for(int i=0; i<g.najlepsze_rozwiazanieM2.size(); i++){
        if(g.najlepsze_rozwiazanieM2[i].start>tmp2){
            file<<"idle_"<<idle2+1<<"_M2, "<<tmp2<<", "<<g.najlepsze_rozwiazanieM2[i].start-tmp2<<"; ";
            idle2++;
            idlet2+=g.najlepsze_rozwiazanieM2[i].start-tmp2;
            tmp2 = g.najlepsze_rozwiazanieM2[i].start;
        }
        if(strcmp(g.najlepsze_rozwiazanieM2[i].typ,"m_")==0){
                m2count++;
            file<<"maint"<<m2count<<"_M2,"<<g.najlepsze_rozwiazanieM2[i].start<<", "<<g.najlepsze_rozwiazanieM2[i].time<<"; ";
            maint2 += g.najlepsze_rozwiazanieM2[i].time;
        }
        else if(strcmp(g.najlepsze_rozwiazanieM2[i].typ,"op")==0)
            file<<"op2_"<<g.najlepsze_rozwiazanieM2[i].zadanie<<", "<<g.najlepsze_rozwiazanieM2[i].start<<", "<<g.najlepsze_rozwiazanieM2[i].time<<"; ";
        tmp2 = g.najlepsze_rozwiazanieM2[i].endtime;
    }


    file<<"\n\n"<<m1count<<", "<<maint1<<endl;
    file<<m2count<<", "<<maint2<<endl;

    file<<"\n"<<idle1<<", "<<idlet1<<endl;
    file<<idle2<<", "<<idlet2;
	file.close();
}
