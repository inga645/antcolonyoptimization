#ifndef GLOBALNE_H_INCLUDED
#define GLOBALNE_H_INCLUDED

const int n=50;
#define liczba_mrowek 500
#define wygladzenie 0.2
#define parowanie 0.1
#define liczba_main 7 // liczba maintenance na ka¿dej maszynie
#define liczba_slepych 100 //liczba slepych mrówek
#define feromon 0.1

#include<vector>
#include<time.h>
#include<algorithm>


struct task{
    char typ[6]; //"op" albo "m" ----> nazwa
   // int numer;  //dla op: 1-2, dla m: nr porzadkowy
    int zadanie; //numer zadania
  //  int gotowy;
    int start;
    int time;
    int endtime;
    int machine;
};

class Globalne{
public:
    int dostepne1[n];
    int dostepne2[n];
    float tablica_przejscM1[n][n];
    float tablica_przejscM2[n][n];
    int czas_maszyny1;
    int czas_maszyny2;
    std::vector<task> najlepsze_rozwiazanieM1;
    std::vector<task> najlepsze_rozwiazanieM2;
    unsigned int najlepszy_czas;
    unsigned int czas_losowy;
    std::vector<task> op1;//maszyna 1
    std::vector<task> op2;//maszyna 2
    std::vector<task> maintenance1;//<czas startu,czas trwania> dla maszyny0
    std::vector<task> maintenance2;//<czas startu,czas trwania> dla maszyny1
    int distr1[500];
    int distr2[500];

    Globalne(){
        najlepszy_czas = 0;
        czas_losowy = 0;
        czas_maszyny1 = 0;
        czas_maszyny2 = 0;
        for(int i=0; i<n; i++){
            for(int j=0; j<n; j++){
                tablica_przejscM1[i][j]=0;
                tablica_przejscM2[i][j]=0;
            }
        }

    }

    ~Globalne(){
        std::vector<task>().swap(najlepsze_rozwiazanieM1);
        std::vector<task>().swap(najlepsze_rozwiazanieM2);
    }
};

#endif // GLOBALNE_H_INCLUDED
