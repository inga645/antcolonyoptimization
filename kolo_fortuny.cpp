#include<iostream>
#include<ctime>
#include<vector>
#include<cstdlib>
#include<fstream>
#include "kolo_fortuny.h"
#include "globalne.h"
using namespace std;

int losuj(Globalne &g, int numer, int punkt) {

	float suma = 0; //suma prawdopodobienstw w tablicy przejsc

    vector <int> kolo_fortuny;//kolo fortuny ma tyle pól ile suma prawdopodobienstw
    vector<int>().swap(kolo_fortuny);

    while(kolo_fortuny.size()==0){
        if(numer==1){
            for (int i = 0; i < n; i++){
                if(g.dostepne1[i]!=-1 && g.dostepne1[i]<=g.czas_maszyny1){
                    suma += g.tablica_przejscM1[punkt][i];
                    kolo_fortuny.push_back(i);
                }
            }
        }
        else{
            for (int i = 0; i < n; i++){
                if(g.dostepne2[i]!=-1 && g.dostepne2[i]<=g.czas_maszyny2){
                    suma += g.tablica_przejscM2[punkt][i];
                    kolo_fortuny.push_back(i);
                }
            }
        }
        if(kolo_fortuny.size()==0 && numer==1){
            g.czas_maszyny1++;
        } else if(kolo_fortuny.size()==0 && numer==2) {
            g.czas_maszyny2++;
        }
    }

    int w;
    if(suma==0 && numer==1){
        w = losuj_slepo(g,1);
    } else if(suma==0 && numer==2) {
        w = losuj_slepo(g,2);
    } else {
        float random;
        do{
            random = suma * (static_cast <float> (rand()) / (static_cast <float> (RAND_MAX)));
        } while (random == 0);
        int id = 0;
        float suma1 = 0;

        while(random > suma1){
            if(numer == 1){
                suma1 += g.tablica_przejscM1[punkt][kolo_fortuny[id]];
            } else {
                suma1 += g.tablica_przejscM2[punkt][kolo_fortuny[id]];
            }
            id++;
        }
        w = kolo_fortuny[id-1];
    }

	vector<int>().swap(kolo_fortuny);
	return w;
}

int losuj_slepo(Globalne &g, int numer) {
	int suma = 0;
	vector<int> tylko_dost;//wektor zawiera tylko dostepne wierzcholki

	while(suma == 0){
        tylko_dost.clear();
        if (numer == 1){
            for (int i = 0; i < n; i++){
                if (g.dostepne1[i] != -1 && g.dostepne1[i] <= g.czas_maszyny1) {
                    suma++;
                    tylko_dost.push_back(i);
                }
            }
        } else {
            for (int i = 0; i < n; i++){
                if(g.dostepne2[i]!=-1 && g.dostepne2[i]<=g.czas_maszyny2){
                    suma++;
                    tylko_dost.push_back(i);
                }
            }
        }
        if(suma==0 && numer==1) g.czas_maszyny1++;
        else if(suma==0 && numer==2) g.czas_maszyny2++;
    }

	int random = rand() % tylko_dost.size();//losuje pozycje w wektorze tylko_dost
	int ret = tylko_dost[random];//zwraca wiercholek,ktorego indeks wylosowalismy
	vector<int>().swap(tylko_dost);
	return ret;
}
