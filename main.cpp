#include<iostream>
#include<time.h>
#include<vector>
#include<stdlib.h>
#include<fstream>
#include <chrono>

#include "kolo_fortuny.h"
#include "generator_instancji.h"
#include"globalne.h"
#include "mrowkowy.h"
#include "generator_rozwiazan.h"
using namespace std;

int main()
{
	srand(time(NULL));//zapewnia unikalnosc losowañ
    ofstream file;
	file.open("liczbazadan90a.txt");
	file << "mrowki slepe: 100; feromon: 0,2; wygladzanie: 0,3 etc" << endl;
    file << "lzadan;czas;funkcja celu" << endl;
    float czas = 60; //czas w sekundach
    int inst = 30;

    for (int a = 1; a <= inst; a++) { //liczba instancji dla jednego parametru
        Globalne *g = new Globalne();
        generuj_instancje(*g);
        zapis(*g,a);

        auto begin = std::chrono::high_resolution_clock::now();
        for (int c = 0; c < liczba_slepych; c++) {
            generuj_rozwiazanie(*g,false,c);//generowanie losowego rozwiazania
            wyparuj(*g);
            wygladz(*g);
        }

        //for (int b = 0; b < liczba_mrowek; b++) {
        int b =0;
        while(1){
            generuj_rozwiazanie(*g,true,b+liczba_slepych);//a- które powtórzenie dla tego zestawu danych b-która mrówka
            wyparuj(*g);
            wygladz(*g);
            auto end = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
            if((float)duration/1000000000 >= czas) {
                //cout << "Czas trwania: "<<(float)duration/1000000000 << endl;
                break;
            }
            b++;
        }

        zapisz_rozw(*g,a);
        file <<"90;10-12;" << g->najlepszy_czas << endl;
        delete g;
    }

	file.close();

	return 0;
}
