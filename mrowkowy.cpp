#include<iostream>
#include<cstdlib>
#include <cmath>
#include "mrowkowy.h"
#include "globalne.h"
using namespace std;


void wyparuj(Globalne &g){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            g.tablica_przejscM1[i][j] *= (1-parowanie);
            g.tablica_przejscM2[i][j] *= (1-parowanie);
        }
    }
}

void wygladz(Globalne &g){
    float suma1 = 0;
    float suma2 = 0;

    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            suma1 += g.tablica_przejscM1[i][j];
            suma2 += g.tablica_przejscM2[i][j];
        }

        float srednia1 = suma1/n;
        float srednia2 = suma2/n;
        float odchylenie;
        for(int j=0; j<n; j++){
            if(g.tablica_przejscM1[i][j]!=0){
                if(g.tablica_przejscM1[i][j] > srednia1){
                    odchylenie = g.tablica_przejscM1[i][j] - srednia1;
                    g.tablica_przejscM1[i][j] = g.tablica_przejscM1[i][j] - wygladzenie*odchylenie;
                } else if(g.tablica_przejscM1[i][j] < srednia1){
                    odchylenie = srednia1 - g.tablica_przejscM1[i][j];
                    g.tablica_przejscM1[i][j] = g.tablica_przejscM1[i][j] + wygladzenie*odchylenie;
                }
            }
            if(g.tablica_przejscM2[i][j]!=0){
                if(g.tablica_przejscM2[i][j] > srednia2){
                    odchylenie = g.tablica_przejscM2[i][j] - srednia2;
                    g.tablica_przejscM2[i][j] = g.tablica_przejscM2[i][j] - wygladzenie*odchylenie;
                } else if(g.tablica_przejscM2[i][j] < srednia2){
                    odchylenie = srednia2 - g.tablica_przejscM2[i][j];
                    g.tablica_przejscM2[i][j] = g.tablica_przejscM2[i][j] + wygladzenie*odchylenie;
                }
            }
        }
    }
}
